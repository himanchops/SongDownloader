import wget
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

def get_download_link(driver):
	b = WebDriverWait(driver, 20).until(lambda driver : driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[4]/ul/li[1]/a'))
	b.click()
	w = WebDriverWait(driver, 20).until(lambda driver : driver.find_element_by_id('download-btn').get_attribute("href"))
	return w

def main():
	a = raw_input('Song Name --> ').strip().split(" ")
	a = "+".join(a)
	url = 'https://musicpleer.audio/#!'+ a

	driver = webdriver.Firefox()
	driver.get(url)
	w = get_download_link(driver)
	r = wget.download(w)
	driver.close()
	
if __name__=='__main__':
	main()
